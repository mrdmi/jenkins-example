var request = require('supertest');
var assert = require('assert');
var app = require('../app.js');
var pjson = require('../package.json');
var version = pjson.version;

describe('GET /test', function() {
  it('respond with OK', function(done) {
    request(app).get('/test').expect('OK', done);
  });
  it('version should not be 1.0.9', function(done) {
    assert.notEqual(version,'1.0.9');
    done();
  });
});
